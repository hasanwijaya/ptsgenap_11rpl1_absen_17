package com.example.ptsgenap_11rpl1_absen_17.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ptsgenap_11rpl1_absen_17.R;
import com.example.ptsgenap_11rpl1_absen_17.model.InventarisModel;

import java.util.ArrayList;

public class InventarisAdapter extends RecyclerView.Adapter<InventarisAdapter.InventarisViewHolder> {
    private ArrayList<InventarisModel> list;
    private OnItemClickCallback onItemClickCallback;

    public void setOnItemClickCallback(OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback;
    }

    public InventarisAdapter(ArrayList<InventarisModel> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public InventarisViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_inventaris, parent, false);
        return new InventarisViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final InventarisViewHolder holder, int position) {
        final InventarisModel inventarisModel = list.get(position);

        holder.tvKode.setText(inventarisModel.getKode());
        holder.tvNama.setText(inventarisModel.getNama());
        holder.tvJenis.setText(inventarisModel.getJenis());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickCallback.onItemClicked(list.get(holder.getAdapterPosition()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class InventarisViewHolder extends RecyclerView.ViewHolder {
        private TextView tvKode, tvNama, tvJenis;

        public InventarisViewHolder(@NonNull View itemView) {
            super(itemView);

            tvKode = itemView.findViewById(R.id.tv_kode);
            tvNama = itemView.findViewById(R.id.tv_nama);
            tvJenis = itemView.findViewById(R.id.tv_jenis);
        }
    }

    public interface OnItemClickCallback {
        void onItemClicked(InventarisModel data);
    }
}
