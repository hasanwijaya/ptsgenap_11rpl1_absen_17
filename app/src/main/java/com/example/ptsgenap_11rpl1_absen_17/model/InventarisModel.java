package com.example.ptsgenap_11rpl1_absen_17.model;

import android.os.Parcel;
import android.os.Parcelable;

public class InventarisModel implements Parcelable {
    String kode, nama, jenis, id;

    public InventarisModel() {

    }

    protected InventarisModel(Parcel in) {
        kode = in.readString();
        nama = in.readString();
        jenis = in.readString();
        id = in.readString();
    }

    public static final Creator<InventarisModel> CREATOR = new Creator<InventarisModel>() {
        @Override
        public InventarisModel createFromParcel(Parcel in) {
            return new InventarisModel(in);
        }

        @Override
        public InventarisModel[] newArray(int size) {
            return new InventarisModel[size];
        }
    };

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(kode);
        dest.writeString(nama);
        dest.writeString(jenis);
        dest.writeString(id);
    }
}
