package com.example.ptsgenap_11rpl1_absen_17.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.ptsgenap_11rpl1_absen_17.R;
import com.example.ptsgenap_11rpl1_absen_17.adapter.InventarisAdapter;
import com.example.ptsgenap_11rpl1_absen_17.model.InventarisModel;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.example.ptsgenap_11rpl1_absen_17.activity.LoginActivity.SHARED_PREFS;

public class MainActivity extends AppCompatActivity {
    private ArrayList<InventarisModel> list = new ArrayList<>();
    private RecyclerView rvInventaris;
    private InventarisAdapter inventarisAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.setTitle("Inventasis Lab RPL");

        rvInventaris = findViewById(R.id.rv_inventaris);
        rvInventaris.setHasFixedSize(true);

        FloatingActionButton fab = findViewById(R.id.fab_add);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, CreateActivity.class);
                startActivity(intent);
            }
        });

        getData();
        showRecycler();
    }

    private void showRecycler() {
        rvInventaris.setLayoutManager(new LinearLayoutManager(this));
        inventarisAdapter = new InventarisAdapter(list);
        rvInventaris.setAdapter(inventarisAdapter);

        inventarisAdapter.setOnItemClickCallback(new InventarisAdapter.OnItemClickCallback() {
            @Override
            public void onItemClicked(InventarisModel data) {
                showSelectedInventaris(data);
            }
        });
    }

    private void showSelectedInventaris(InventarisModel inventarisModel) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra("Item Data", inventarisModel);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.action_logout) {
            SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();

            editor.clear();
            editor.apply();

            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    public void getData() {
        AndroidNetworking.get("http://192.168.43.21/inventaris-lab-rpl-api/getAllBarang.php")
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            boolean status = response.getBoolean("status");

                            if (status) {
                                JSONArray data = response.getJSONArray("data");
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject barang = data.getJSONObject(i);
                                    InventarisModel item = new InventarisModel();
                                    item.setId(barang.getString("b_id"));
                                    item.setKode(barang.getString("b_kode"));
                                    item.setNama(barang.getString("b_nama"));
                                    item.setJenis(barang.getString("b_jenis"));
                                    list.add(item);
                                }

                                inventarisAdapter.notifyDataSetChanged();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }
}
