package com.example.ptsgenap_11rpl1_absen_17.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.ptsgenap_11rpl1_absen_17.R;
import com.example.ptsgenap_11rpl1_absen_17.model.InventarisModel;

import org.json.JSONException;
import org.json.JSONObject;

public class DetailActivity extends AppCompatActivity {
    private EditText edtkode, edtNama, edtJenis;
    private Button btnUpdate, btnDelete;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.setTitle("Detail Data");

        edtkode = findViewById(R.id.edt_kode);
        edtNama = findViewById(R.id.edt_nama);
        edtJenis = findViewById(R.id.edt_jenis);
        btnUpdate = findViewById(R.id.btn_update);
        btnDelete = findViewById(R.id.btn_delete);

        Intent intent = getIntent();
        final InventarisModel itemData = intent.getParcelableExtra("Item Data");

        edtkode.setText(itemData.getKode());
        edtNama.setText(itemData.getNama());
        edtJenis.setText(itemData.getJenis());

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateData(itemData.getId());
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteData(itemData.getId());
            }
        });
    }

    private void deleteData(String id) {
        AndroidNetworking.post("http://192.168.43.21/inventaris-lab-rpl-api/deleteBarang.php")
                .addBodyParameter("id", id)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            boolean status = response.getBoolean("status");
                            if (status) {
                                Toast.makeText(DetailActivity.this, "Delete data success", Toast.LENGTH_SHORT).show();

                                Intent intent = new Intent(DetailActivity.this, MainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }

    private void updateData(String id) {
        String inputKode = edtkode.getText().toString().trim();
        String inputNama = edtNama.getText().toString().trim();
        String inputJenis = edtJenis.getText().toString().trim();

        boolean isEmpty = false;

        if (inputKode.isEmpty()) {
            isEmpty = true;
            edtkode.setError("Kode harus diisi");
        }

        if (inputNama.isEmpty()) {
            isEmpty = true;
            edtNama.setError("Nama harus diisi");
        }

        if (inputJenis.isEmpty()) {
            isEmpty = true;
            edtJenis.setError("Jenis harus diisi");
        }

        if (!isEmpty) {
            AndroidNetworking.post("http://192.168.43.21/inventaris-lab-rpl-api/updateBarang.php")
                    .addBodyParameter("id", id)
                    .addBodyParameter("kode", inputKode)
                    .addBodyParameter("nama", inputNama)
                    .addBodyParameter("jenis", inputJenis)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                boolean status = response.getBoolean("status");
                                if (status) {
                                    Toast.makeText(DetailActivity.this, "Update data success", Toast.LENGTH_SHORT).show();

                                    Intent intent = new Intent(DetailActivity.this, MainActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError error) {
                            // handle error
                        }
                    });
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
