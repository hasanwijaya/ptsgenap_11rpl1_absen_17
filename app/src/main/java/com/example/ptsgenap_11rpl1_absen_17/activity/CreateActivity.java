package com.example.ptsgenap_11rpl1_absen_17.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.example.ptsgenap_11rpl1_absen_17.R;

import org.json.JSONException;
import org.json.JSONObject;

public class CreateActivity extends AppCompatActivity {
    private EditText edtkode, edtNama, edtJenis;
    private Button btnAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        this.setTitle("Add Data");

        edtkode = findViewById(R.id.edt_kode);
        edtNama = findViewById(R.id.edt_nama);
        edtJenis = findViewById(R.id.edt_jenis);
        btnAdd = findViewById(R.id.btn_add);

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addData();
            }
        });
    }

    private void addData() {
        String inputKode = edtkode.getText().toString().trim();
        String inputNama = edtNama.getText().toString().trim();
        String inputJenis = edtJenis.getText().toString().trim();

        boolean isEmpty = false;

        if (inputKode.isEmpty()) {
            isEmpty = true;
            edtkode.setError("Kode harus diisi");
        }

        if (inputNama.isEmpty()) {
            isEmpty = true;
            edtNama.setError("Nama harus diisi");
        }

        if (inputJenis.isEmpty()) {
            isEmpty = true;
            edtJenis.setError("Jenis harus diisi");
        }

        if(!isEmpty) {
            AndroidNetworking.post("http://192.168.43.21/inventaris-lab-rpl-api/insertBarang.php")
                    .addBodyParameter("kode", inputKode)
                    .addBodyParameter("nama", inputNama)
                    .addBodyParameter("jenis", inputJenis)
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                boolean status = response.getBoolean("status");
                                if (status){
                                    Toast.makeText(CreateActivity.this, "Add data success", Toast.LENGTH_SHORT).show();

                                    Intent intent = new Intent(CreateActivity.this, MainActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        @Override
                        public void onError(ANError error) {
                            // handle error
                        }
                    });
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
